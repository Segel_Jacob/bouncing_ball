Instructions for installing the project (Bouncing_ball):
1. Open bitbucket and connect to the project repository
2. Click + in the global sidebar and select 'Clone this repository' under 'Get to work'
3. Click on 'Clone the repository using Sourcetree'
4. Browse for a local path to clone the repository into
5. Click on 'Clone'

For this project, I have chosen to with the Apache 2.0 License. Since this project does
not contain any proprietary information, its usage and distribution need not be limited.
It is a permissive license that does not require further modifications of the project
to be released under the same license, making it more flexible. Changes made, however
need to be explicitly listed which makes it easier to track.

