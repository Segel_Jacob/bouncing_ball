﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Audio;

namespace BouncingBall
{
    public class CollisionDetection : GameComponent
    {
        private Game1 parent;
        private Ball ball;
        private Bat bat;
        private Explosion explosion;

        public CollisionDetection(Game game,
            Ball ball,
            Bat bat,
            Explosion explosion) : base(game)
        {
            parent = (Game1)game;
            this.ball = ball;
            this.bat = bat;
            this.explosion = explosion;
        }
        public override void Update(GameTime gameTime)
        {
            Rectangle batRect = bat.GetBound();
            Rectangle ballRect = ball.GetBound();
            if (batRect.Intersects(ballRect))
            {                
                ball.BounceBottom();
                explosion.StartAnimation(ballRect.Center.ToVector2());
            }
            base.Update(gameTime);
        }
    }
}
