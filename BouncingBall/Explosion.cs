﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Audio;

namespace BouncingBall
{
    public class Explosion : DrawableGameComponent
    {
        private Game1 parent;
        private Texture2D tex;
        private int width;
        private int height;
        private int currentX;
        private int currentY;
        private Vector2 position;

        public Explosion(Game game,
            string image,
            int rows, 
            int cols) : base(game)
        {
            parent = (Game1)game;
            this.position = Vector2.Zero;
            tex = parent.Content.Load<Texture2D>(image);
            this.width = tex.Width / cols;
            this.height = tex.Height / rows;
            currentX = 0;
            currentY = 0;
        }

        public override void Update(GameTime gameTime)
        {
            currentX += width;
            if (currentX >= tex.Width)
            {
                currentX = 0;
                currentY += height;
                if (currentY >= tex.Height)
                {
                    this.Enabled = false;
                    this.Visible = false;
                }
            }
            base.Update(gameTime);
                
        }

        public override void Draw(GameTime gameTime)
        {
            Rectangle sourceRectangle = new Rectangle(currentX, currentY, width, height);
            
            parent.Sprite.Begin();
            parent.Sprite.Draw(tex, position, sourceRectangle, Color.White);
            parent.Sprite.End();
            base.Draw(gameTime);
        }
        public void StartAnimation(Vector2 position)
        {
            this.position = position;
            currentX = 0;
            currentY = 0;
            this.Enabled = true;
            this.Visible = true;
        }
    }
}
