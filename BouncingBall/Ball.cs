﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Audio;

namespace BouncingBall
{
    public class Ball : DrawableGameComponent
    {
        Game1 parent;
        private Texture2D tex;
        private Vector2 speed;
        private Vector2 position;
        private Vector2 stage;
        private SoundEffect hitSound;
        private SoundEffect missSound;

        public Ball(Game game,
            string imageName,
            Vector2 speed,
            Vector2 stage,
            string hitSoundName,
            string missSoundName
            ) : base(game)
        {
            this.parent = (Game1)game;
            this.tex = parent.Content.Load<Texture2D>(imageName);
            this.position = new Vector2(stage.X / 2 - tex.Width / 2,
                                        stage.Y / 2  - tex.Height / 2);
            this.speed = speed;
            this.stage = stage;
            hitSound = parent.Content.Load<SoundEffect>(hitSoundName);
            missSound = parent.Content.Load<SoundEffect>(missSoundName);
        }

        public override void Draw(GameTime gameTime)
        {
            parent.Sprite.Begin();
            parent.Sprite.Draw(tex, position, Color.White);
            parent.Sprite.End();
            base.Draw(gameTime);
        }

        public override void Initialize()
        {
            base.Initialize();
        }

        public override void Update(GameTime gameTime)
        {
            position += speed;
            //right wall
            if (position.X + tex.Width >= stage.X)
            {
                speed.X = -Math.Abs(speed.X);
                hitSound.Play();
            }
            //left wall
            if (position.X <= 0)
            {
                speed.X = Math.Abs(speed.X);
                hitSound.Play();
            }
            //top wall
            if (position.Y <= 0)
            {
                speed.Y = Math.Abs(speed.Y);
                hitSound.Play();
            }
            //bottom wall, will loose the ball.
            if (position.Y >= stage.Y)
            {
                // speed.Y = -Math.Abs(speed.Y);
                // hitSound.Play();
                this.Enabled = false;
                missSound.Play();
            }
            base.Update(gameTime);
        }

        public Rectangle GetBound()
        {
            return new Rectangle(
                (int)position.X, 
                (int)position.Y, 
                tex.Width, 
                tex.Height);
        }
        public void BounceBottom()
        {
            speed.Y = -Math.Abs(speed.Y);
            hitSound.Play();
        }
        public void Restart()
        {
            this.position = new Vector2(
                stage.X / 2 - tex.Width / 2,
                stage.Y / 2 - tex.Height / 2);
            this.speed = new Vector2(5, -5);
            this.Enabled = true;
            this.Visible = true;
        }
    }
}
// missSound.Play();
// this.Enabled = false;
