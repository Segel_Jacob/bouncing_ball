﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Audio;

namespace BouncingBall
{
    public class MenuScene : GameScene
    {
        private SpriteFont regularFont;
        private SpriteFont hilightFont;
        private Color regularColor = Color.Black;
        private Color hilightColor = Color.Red;

        private List<string> menuItems;
        public int selectedIndex;

        private Vector2 position;

        private KeyboardState oldState; 

        public MenuScene(Game game,
            List<string> menuItems) : base(game)
        {
            this.regularFont = parent.Content.Load<SpriteFont>("Fonts/regularFont");
            this.hilightFont = parent.Content.Load<SpriteFont>("Fonts/hilightFont");
            this.menuItems = menuItems;  
            position = new Vector2(parent.Stage.X / 2, parent.Stage.Y / 2);
        }

        public override void Update(GameTime gameTime)
        {
            KeyboardState ks = Keyboard.GetState();
            if (oldState.IsKeyUp(Keys.Down) && ks.IsKeyDown(Keys.Down))
            {
                selectedIndex = MathHelper.Clamp(selectedIndex + 1, 0, menuItems.Count-1);
            }
            if (oldState.IsKeyDown(Keys.Up) && ks.IsKeyUp(Keys.Up) )
            {
                selectedIndex = MathHelper.Clamp(selectedIndex - 1, 0, menuItems.Count-1);
            }
            if (oldState.IsKeyUp(Keys.Enter) && ks.IsKeyDown(Keys.Enter) )
            {
                parent.Notify(this, menuItems[selectedIndex]);
            }
            oldState = ks;

            base.Update(gameTime);
        }

        public override void Draw(GameTime gameTime)
        {
            Vector2 tempPos = position;
            parent.Sprite.Begin();
            for (int i = 0; i < menuItems.Count; i++)
            {
                if (selectedIndex == i)
                {
                    parent.Sprite.DrawString(hilightFont, menuItems[i], tempPos, hilightColor);
                    tempPos.Y += hilightFont.LineSpacing;
                }
                else
                {
                    parent.Sprite.DrawString(regularFont, menuItems[i], tempPos, regularColor);
                    tempPos.Y += regularFont.LineSpacing;
                }
            }
            parent.Sprite.End();

            base.Draw(gameTime);
        }
    }
}
