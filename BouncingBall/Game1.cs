﻿using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Audio;

namespace BouncingBall
{
    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    public class Game1 : Game
    {
        GraphicsDeviceManager graphics;
        public GraphicsDeviceManager Graphics { get => graphics; }
        SpriteBatch spriteBatch;
        public SpriteBatch Sprite { get => spriteBatch; }
        Vector2 stage;
        public Vector2 Stage { get => stage; }

        private GameScene currentScene;

        // menu items we want to display
        private List<string> menuItems = 
            new List<string>{
                "Start Game",
                "Help",
                "High Score",
                "Credit",
                "Quit" };

        //declare all the scenes here
        private MenuScene menuScene;
        private ActionScene actionScene;
        private HelpScene helpScene;


        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here
            IsMouseVisible = true;
            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);
            stage = new Vector2(
                graphics.PreferredBackBufferWidth,
                graphics.PreferredBackBufferHeight);

            menuScene = new MenuScene(this, menuItems);
            this.Components.Add(menuScene);
            currentScene = menuScene;
            currentScene.show();
            
            //create other scenes here and add to component list
            actionScene = new ActionScene(this);
            this.Components.Add(actionScene);

            helpScene = new HelpScene(this);
            this.Components.Add(helpScene);
            base.LoadContent();
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// game-specific content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.Beige);
            base.Draw(gameTime);
        }

        public void Notify(GameScene sender, string action)
        {
            currentScene.hide();
            if (sender is MenuScene)
            {
                switch (action)
                {
                    case "Start Game":
                        currentScene = actionScene;
                        break;
                    case "Help":
                        currentScene = helpScene;
                        break;
                    case "Quit":
                        Exit();
                        break;
                }
            }
            else if (sender is ActionScene)
            {
                currentScene = menuScene;
            }
            else if (sender is HelpScene)
            {
                currentScene = menuScene;
            }
            currentScene.show();
        }
    }
}
