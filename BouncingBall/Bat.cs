﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Audio;

namespace BouncingBall
{
    public class Bat : DrawableGameComponent
    {
        Game1 parent;
        private Texture2D tex;
        private Vector2 speed;
        private Vector2 position;
        private Vector2 stage;

        public Bat(Game game, 
            string imageName, 
            Vector2 speed, 
            Vector2 stage) : base(game)
        {
            this.parent = (Game1)game;
            this.tex = parent.Content.Load<Texture2D>(imageName);
            this.position = new Vector2(stage.X / 2 - tex.Width / 2, 
                                        stage.Y - tex.Height);
            this.speed = speed;
            this.stage = stage;
        }

        public override void Update(GameTime gameTime)
        {
            KeyboardState ks = Keyboard.GetState();
            if (ks.IsKeyDown(Keys.Right))
            {
                position += speed;
            }
            if (ks.IsKeyDown(Keys.Left))
            {
                position -= speed;
            }

            position.X = MathHelper.Clamp(
                position.X, 0, stage.X - tex.Width);

            base.Update(gameTime);
        }

        public override void Draw(GameTime gameTime)
        {
            parent.Sprite.Begin();
            parent.Sprite.Draw(tex, position, Color.White);
            parent.Sprite.End();
            base.Draw(gameTime);
        }

        public Rectangle GetBound()
        {
            return new Rectangle(
                (int)position.X, 
                (int)position.Y, 
                tex.Width, 
                tex.Height);
        }
    }
}
