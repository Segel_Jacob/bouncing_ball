﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Audio;

namespace BouncingBall
{
    public class ActionScene : GameScene
    {
        Bat bat;
        Ball ball;
        KeyboardState oldstate;

        public ActionScene(Game game) : base(game)
        {
            ball = new Ball(parent,     // game
                "Images/Ball",          // image
                new Vector2(5, -5),     // speed
                parent.Stage,           // stage
                "Music/click",          // hitSoundName
                "Music/applause1");     // missSoundName
            this.Components.Add(ball);

            bat = new Bat(parent,       // game
                "Images/Bat",           // image
                new Vector2(4, 0),      // speed
                parent.Stage);          // stage
            this.Components.Add(bat);

            Explosion explosion = new Explosion(parent,
                "Images/explosion", 5, 5);
            this.Components.Add(explosion);

            CollisionDetection cd =
                new CollisionDetection(
                    parent, ball, bat, explosion);
            this.Components.Add(cd);
        }

        public override void Draw(GameTime gameTime)
        {
            base.Draw(gameTime);
        }

        public override void Update(GameTime gameTime)
        {
            KeyboardState ks = Keyboard.GetState();
            if (ks.IsKeyDown(Keys.Escape) && oldstate.IsKeyUp(Keys.Escape))
            {
                parent.Notify(this, "escape");
            }
            if (ball.Enabled == false)
            {
                parent.Notify(this, "gameover");
            }
            oldstate = ks;
            base.Update(gameTime);
        }
    
        protected override void LoadContent()
        {
            base.LoadContent();
        }
        public override void show()
        {
            Rectangle brect = ball.GetBound();
            // Restart ball if the game ended before
            if (brect.Y+brect.Width >= parent.Stage.Y)
                ball.Restart();
            base.show();
        }
    }
}
